import React from 'react'
import { Provider } from 'react-redux'
import { RouterProvider } from 'react-router-dom'

import '../styles/globals.css'
import '../styles/kanban.css'
import Router from './Router'
import useAppContext, { StateContext, DispatchContext } from '../context'
import store from '../store'

export default function App () {
  const [combinedDispatch, combinedState] = useAppContext()
  return (
    <Provider store={store}>
      <StateContext.Provider value={combinedState}>
        <DispatchContext.Provider value={combinedDispatch}>
          <RouterProvider router={Router}/>
        </DispatchContext.Provider>
      </StateContext.Provider>
    </Provider>
  )
}
