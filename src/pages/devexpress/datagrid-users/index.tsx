// @ts-nocheck

import ContextMenu from 'devextreme-react/context-menu'
import { DataGrid, Column } from 'devextreme-react/data-grid'
import DropDownButton from 'devextreme-react/drop-down-button'
import CustomStore from 'devextreme/data/custom_store'
import _ from 'lodash'
import React, { useState, useMemo } from 'react'

import Layout from '../../../components/Layout'
import { generateGridSettings, generateColumnData } from '../../../misc/mockdata/gridcolumns'
import userSettings from '../../../misc/mockdata/users'

const handleContextMenuClick = (e) => {
  if (!e.items) e.items = []
  e.items.push({
    text: 'Highlight',
    items: [
      {
        text: 'red',
        onItemClick: () => e.targetElement.style.setProperty('background-color', 'red')
      },
      {
        text: 'yellow',
        onItemClick: () => e.targetElement.style.setProperty('background-color', 'yellow')
      }
    ]
  })
  e.items.push({
    text: 'Text',
    items: [
      {
        text: 'Copy',
        onItemClick: () => navigator.clipboard.writeText(e.targetElement.innerText)
      }
    ]
  })
}

const customDataSource = new CustomStore({
  key: 'EmployeeID',
  load: (loadOptions: any) => {
    const { sort } = loadOptions
    let loadUrl = `${process.env.REACT_APP_PUBLIC_SERVER}/api/employees`
    if (sort && _.isArray(sort) && sort.length) {
      const col = sort[0].selector
      const order = sort[0].desc ? 'desc' : 'asc'
      loadUrl = `${loadUrl}?col=${col}&order=${order}`
    }
    return fetch(loadUrl)
      .then(response => response.json())
  },
  update: (key, values) => console.log(key, values),
  insert: (values) => console.log(values)
})

const Datagrid = () => {
  const [selectedUser, setSelectedUser] = useState<'karel'|'franta'>('karel')

  const selectedUserSettings = userSettings[selectedUser]
  const gridSettings = useMemo(() => generateGridSettings(selectedUserSettings), [selectedUserSettings])
  const columnData = useMemo(() => generateColumnData(selectedUserSettings), [selectedUserSettings])

  // console.log("gridSettings",gridSettings)

  return (
    <Layout>
      <h2>Datagrid</h2>
      <DropDownButton
        text="Select user"
        icon="user"
        items={['franta', 'karel']}
        style={{ marginBottom: 20 }}
        onItemClick={({ itemData }) => setSelectedUser(itemData)}
        selectedItem="franta"
    />
      <h5>Selected user: {selectedUserSettings.name} </h5>
      <DataGrid
    id="grid"
    dataSource={customDataSource}
    onContextMenuPreparing={handleContextMenuClick}
    {...gridSettings}
    >
        {columnData.map((item) => {
          const { colAccess, ...props } = item
          if (!colAccess.includes(selectedUserSettings.columnAccess)) {
            return null
          } else {
            return (
              <Column
                key={props.dataField}
                {...props}
            />)
          }
        })}
      </DataGrid>
      <ContextMenu
    dataSource={[]}
    target="#grid"
    onItemClick={(e) => console.log('EEE')}
    />
    </Layout>
  )
}

export default Datagrid
