import React from 'react'
import { createBrowserRouter } from 'react-router-dom'
import DataGrid from './devexpress/datagrid'
import DataGridLarge from './devexpress/datagrid-large'
import DataGridUsers from './devexpress/datagrid-users'
import Form from './devexpress/form'
import FormLayout from './devexpress/form-layout'
import Home from './devexpress/home'
import Kanban from './devexpress/kanban'
import Settings from './devexpress/settings'

const Router = createBrowserRouter([
  { path: '/', element: <Home/> },
  { path: '/settings', element: <Settings/> },
  { path: '/datagrid', element: <DataGrid/> },
  { path: '/datagrid-users', element: <DataGridUsers/> },
  { path: '/datagrid-large', element: <DataGridLarge/> },
  { path: '/form', element: <Form/> },
  { path: '/form-layout', element: <FormLayout/> },
  { path: '/kanban', element: <Kanban/> }
])

export default Router
