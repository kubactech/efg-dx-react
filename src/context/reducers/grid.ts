import { Action } from './../index';
import React from "react"

const initialState = {
    fixedNameCol:false,
    allowColumnReordering:true,
    columnAutoWidth:false,
    columnFixing:false,
    columnChooser:false,
    searchPanel:false,
    allowAdding:false,
}

const ACTIONS = {
    SET_SETTINGS:'SET_SETTINGS',
}

export const setGridSettings = (dispatch:React.Dispatch<Action<any>>, payload:Record<string,boolean>) => dispatch({type:ACTIONS.SET_SETTINGS, payload})

const reducer = (state = initialState, action:Action<any>) =>{
    switch (action.type) {
        case ACTIONS.SET_SETTINGS:
            return { ...action.payload}            
    
        default:
            return state
    }
}

// eslint-disable-next-line import/no-anonymous-default-export
export default ()=> React.useReducer(reducer,initialState)