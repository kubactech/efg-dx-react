export const KarelUser = {
    user_id:"karel",
    name:"Karel Svobodny",
    role:"admin",
    columnAccess:'full',
    gridFeatures : {
        columnChooser:true,
        columnFixing:true,
        editing:{
            editing:true,
            adding:true,
            deleting:true
        },
        stateStoring:false,
        grouping: true
    }
}

export const FrantaUser = {
    user_id:"frantisek",
    name:"Frantisek Novak",
    role:"user",
    columnAccess:'limited',
    gridFeatures : {
        columnChooser:true,
        columnFixing:true,
        editing:{
            editing:false,
            adding:false,
            deleting:false
        },
        stateStoring:false,
        grouping: true
    }
}

const userSettings = {
    karel:KarelUser,
    franta: FrantaUser
}

export default userSettings