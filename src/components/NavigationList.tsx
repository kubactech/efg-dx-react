import List from 'devextreme-react/list.js'
import React from 'react'
import { useNavigate } from 'react-router-dom'

export const navigation = [
  { id: 1, text: 'Form', icon: 'fields', link: '/form' },
  { id: 2, text: 'Form with layout', icon: 'fields', link: '/form-layout' },
  { id: 3, text: 'Data Grid', icon: 'splitcells', link: '/datagrid' },
  { id: 4, text: 'Data Grid with users', icon: 'splitcells', link: '/datagrid-users' },
  { id: 5, text: 'LARGE Datagrid', icon: 'splitcells', link: '/datagrid-large' },
  { id: 6, text: 'Settings', icon: 'preferences', link: '/settings' }
]

const NavigationList = () => {
  const navigate = useNavigate()
  return (
    <div className='list' style={{ width: '200px' }}>
      <List
          dataSource={navigation}
          hoverStateEnabled={true}
          activeStateEnabled={true}
          focusStateEnabled={false}
          className="panel-list dx-theme-accent-as-background-color"
          // @ts-ignore
          onItemClick={(e) => navigate(e.itemData?.link)}
          selectionMode="single"
          />
    </div>
  )
}

export default NavigationList
